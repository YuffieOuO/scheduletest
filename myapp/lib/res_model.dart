// import 'dart:convert';

class ResBindUser {
  bool error;
  String reason;
  String? token;

  ResBindUser({
    required this.error,
    required this.reason,
    required this.token,
  });

  factory ResBindUser.fromMap(Map<String, dynamic> json) => ResBindUser(
        error: json['error'],
        reason: json['reason'],
        token: json['token'],
      );
}

class GetUserProfile {
  String id;
  String username;
  String? nickname;
  String? employee_id;
  String group_id;
  String? token;

  GetUserProfile({
    required this.id,
    required this.username,
    required this.nickname,
    required this.employee_id,
    required this.group_id,
    required this.token,
  });

  factory GetUserProfile.fromMap(Map<String, dynamic> json) => GetUserProfile(
      id: json['id'],
      username: json['username'],
      nickname: json['nickname'],
      employee_id: json['employee_id'],
      group_id: json['group_id'],
      token: json['token']);
}

class GetUserLogs {
  GetUserLogs({
    required this.logs,
    required this.page,
    this.token,
  });

  List<Log> logs;
  Page page;
  String? token;

  factory GetUserLogs.fromJson(Map<String, dynamic> json) => GetUserLogs(
        logs: List<Log>.from(json['logs'].map((x) => Log.fromJson(x))),
        page: Page.fromJson(json['page']),
        token: json['token'],
      );
}

class Log {
  Log({
    required this.username,
    required this.employeeId,
    required this.datetime,
    required this.type,
    required this.id,
  });

  String username;
  String employeeId;
  DateTime datetime;
  String type;
  String id;

  factory Log.fromJson(Map<String, dynamic> json) => Log(
        username: json['username'],
        employeeId: json['employee_id'],
        datetime: DateTime.parse(json['datetime']),
        type: json['type'],
        id: json['id'],
      );
}

class Page {
  Page({
    required this.total,
    required this.page,
    required this.per,
  });

  int total;
  int page;
  int per;

  factory Page.fromJson(Map<String, dynamic> json) => Page(
        total: json['total'],
        page: json['page'],
        per: json['per'],
      );
}

class ResPunch {
  bool error;
  DateTime reason;
  String? token;

  ResPunch({
    required this.error,
    required this.reason,
    required this.token,
  });

  factory ResPunch.fromMap(Map<String, dynamic> json) => ResPunch(
        error: json['error'],
        reason: DateTime.parse(json['reason']),
        token: json['token'],
      );
}
