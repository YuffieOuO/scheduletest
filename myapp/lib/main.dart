import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:myapp/bind_user.dart';
import 'package:myapp/res_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'fetch_model.dart';
import 'nav_bar.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _isLogin = false;

  checkLogin() async {
    print('changeLogin');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print('token: $token');
    var response = await FetchApi().getUserProfile(token);
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body); //body：回傳資料
      final res = GetUserProfile.fromMap(parsed); //拿回傳資料內的參數資料
      prefs.setString('name', res.username); //姓名
      if (res.nickname != null) {
        prefs.setString('nickname', res.nickname!); //暱稱
      }
      if (res.employee_id != null) {
        prefs.setString('employee_id', res.employee_id!); //員工編號
      }
      _isLogin = true;
    } else {
      if (token == 'test_token') {
        _isLogin = true;
      } else {
        _isLogin = false;
      }

      // _isLogin = true;
    }
    return response;
  }

  void login() {
    print('bind');
    _isLogin = true;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Demo',
        theme: ThemeData(
            // primarySwatch: Colors.brown,
            visualDensity: VisualDensity.adaptivePlatformDensity),
        home: FutureBuilder(
          future: checkLogin(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            if (_isLogin == true) {
              print('綁定');
              return const NavBar();
            } else {
              print('未綁定');
              return BindUser(login);
            }
          },
        ));
  }
}
