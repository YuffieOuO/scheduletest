import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  String? name;
  String? nickName;
  String? employee_id;

  @override
  void initState() {
    getUserInfo();
    super.initState();
  }

  void getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final getName = prefs.getString('name');
    final getNickName = prefs.getString('nickname');
    final getEmployeeId = prefs.getString('employee_id');
    setState(() {
      name = getName;
      nickName = getNickName;
      employee_id = getEmployeeId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Setting', style: TextStyle(fontSize: 26)),
      ),
      body: Container(
          margin: const EdgeInsets.only(top: 10, left: 10),
          alignment: Alignment.topCenter,
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    textSection('姓名', name),
                    textSection('暱稱', nickName),
                  ]),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  textSection('員工編號', employee_id),
                ],
              ),
            ],
          )),
    );
  }
}

Widget textSection(label, key) {
  return SizedBox(
      height: 30,
      width: 180,
      child: Text(
        '$label: $key',
        style: const TextStyle(
          fontSize: 20,
        ),
      ));
}
