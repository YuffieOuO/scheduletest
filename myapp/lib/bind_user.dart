import 'dart:convert';
import 'package:myapp/res_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'fetch_model.dart';

class BindUser extends StatefulWidget {
  final VoidCallback callback;
  const BindUser(this.callback, {Key? key}) : super(key: key);

  @override
  _BindUserState createState() => _BindUserState();
}

class _BindUserState extends State<BindUser> {
  var userCodeVerification = true;

  final TextEditingController userCode = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      width: 340,
      height: 200,
      margin: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: testLogin,
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    primary: const Color.fromARGB(255, 139, 133, 139),
                    textStyle: const TextStyle(fontSize: 20)),
                child: const Text('訪客登入'),
              ),
            ],
          ),
          Text(userCodeVerification ? '' : '無效的登入代碼',
              style: const TextStyle(
                  color: Colors.red,
                  fontSize: 18,
                  letterSpacing: 1,
                  fontWeight: FontWeight.w500)),
          TextField(
            style: const TextStyle(fontSize: 20),
            decoration: const InputDecoration(hintText: '請輸入登入代碼'),
            controller: userCode,
          ),
          Container(
            margin: const EdgeInsets.only(top: 20),
            child: ElevatedButton(
              onPressed: bind,
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  textStyle: const TextStyle(fontSize: 20, letterSpacing: 2)),
              child: const Text('確認'),
            ),
          ),
        ],
      ),
    )));
  }

  void testLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("登入");
    prefs.setString('token', "test_token");
    widget.callback();
    setState(() {});
  }

  void bind() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String code = userCode.text;
    var response = await FetchApi().bindUser(code);
    print('bindStatus: ${response.statusCode}');
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body); //body：回傳資料
      final res = ResBindUser.fromMap(parsed); //拿回傳資料內的參數資料
      print('res:');
      print(res);
      // 檢查token
      print('resToken: ${res.token}');
      if (res.token != null) {
        print('get token');
        prefs.setString('token', res.token!); //將 token 存起來
        userCodeVerification = true;
        widget.callback();
        setState(() {});
      } else {
        print('bindStatus: != 200');
      }
    } else {
      print('沒有成功');
      userCodeVerification = false;
      setState(() {});
    }
  }
}
