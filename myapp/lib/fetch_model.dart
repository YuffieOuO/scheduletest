import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'res_model.dart';

class FetchApi {
  var body = <String, dynamic>{};

  Uri setUrl(path) {
    //http://www.zencher.com:5000
    final url = Uri.parse('http://www.zencher.com:5000/$path');
    final newUrl =
        Uri(scheme: 'http', host: 'www.zencher.com', port: 5000, path: path);
    //驗證網址
    if (newUrl == url) {
      // print('path user: Success');
      return newUrl;
    } else {
      throw 'path: Fail';
    }
  }

  Future<http.Response> bindUser(account) async {
    //body 回傳 account（user code）
    body['account'] = account;
    //發 account，成功會回傳資料
    return http.post(setUrl('user'), body: body);
  }

  getUserProfile(token) async {
    return http
        .get(setUrl('user'), headers: {'Authorization': 'Bearer $token'});
  }

  Future<GetUserLogs> getUserLog() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    var userLogs;
    var res = await http.get(
        Uri.parse('http://www.zencher.com:5000/user/log?page=1&per=100'),
        headers: {'Authorization': 'Bearer $token'});
    if (res.statusCode == 200) {
      final parsed = json.decode(res.body); //body：回傳資料
      // final resUserPro = GetUserLogs.fromJson(parsed);
      // userLogs = resUserPro;
      userLogs = GetUserLogs.fromJson(parsed);
      print('userLogs: ${userLogs.logs}');
      // if (userLogs.token ){

      // }
    }
    return userLogs;
  }

  Future<http.Response> punchInOrOut(longitude, latitude, type) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    body['longitude'] = longitude.toString();
    body['latitude'] = latitude.toString();
    body['punch_in_out_type'] = type.toString(); //0上,1下
    return http.post(setUrl('user/log'),
        body: body, headers: {'Authorization': 'Bearer $token'});
  }
}
