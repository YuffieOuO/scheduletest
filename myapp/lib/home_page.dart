import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:myapp/fetch_model.dart';
import 'package:myapp/res_model.dart';
import 'draw_circle.dart';
import 'package:geolocator/geolocator.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _text = '尚未打卡';
  late Position _currentPosition;
  Color _color = Colors.green.shade200;
  late String _date;
  late String _time;
  late String _type;
  bool typeState = false;
  @override
  void initState() {
    _determinePosition();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget buttonSection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _buildButtonColumn('上班', 0),
        _buildButtonColumn('下班', 1),
      ],
    );

    Widget textSection = Container(
      // color: Colors.black38,
      width: 400,
      height: 400,
      margin: const EdgeInsets.only(bottom: 10),
      child: Stack(alignment: Alignment.center, children: [
        CustomPaint(
          painter: DrawCircle(_color),
        ),
        typeState
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(_date, textScaleFactor: 2),
                  Text(
                    _time,
                    textScaleFactor: 4.5,
                    style: const TextStyle(color: Colors.grey),
                  ),
                  Text(
                    _type,
                    textScaleFactor: 2,
                  ),
                  Text(
                    _text,
                    textScaleFactor: 2.4,
                  )
                ],
              )
            : Text(
                _text,
                textScaleFactor: 2.4,
              ),
      ]),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          child: textSection,
        ),
        SizedBox(
          child: buttonSection,
        )
      ],
    );
  }

  Column _buildButtonColumn(String label, int type) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 48,
          width: 100,
          child: ElevatedButton(
            onPressed: () => _getCurrentLocation(label, type),
            style: ElevatedButton.styleFrom(
                textStyle: const TextStyle(
              fontSize: 24,
              letterSpacing: 2.0,
              fontWeight: FontWeight.w600,
            )),
            child: Text(label),
          ),
        ),
      ],
    );
  }

  _getCurrentLocation(String label, int type) async {
    print('打卡');
    _color = Colors.yellow.shade200;
    setState(() {});
    var position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best,
        forceAndroidLocationManager: true);
    _currentPosition = position;
    print(_currentPosition);
    final response = await FetchApi().punchInOrOut(
        _currentPosition.longitude, _currentPosition.latitude, type);
    print(response);
    print('statusCode: ${response.statusCode}');
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      final res = ResPunch.fromMap(parsed).reason;
      print(res.day);
      _color = Colors.green.shade200;
      _text = '打卡成功';
      final year = res.year;
      final month = res.month;
      final day = res.day;
      final hour = res.hour;
      final minute = res.minute;
      final second = res.second;
      _date = '$year/$month/$day';
      // _date = 'XXXX/XX/XX';
      _time = '$hour:$minute:$second';
      // _time = 'XX:XX:XX';
      _type = label;
      typeState = true;
      setState(() {});
    } else {
      print(response.statusCode);
    }
  }

  Future<Position> _determinePosition() async {
    print('請求權限');
    bool serviceEnabled;
    LocationPermission permission;
    // LocationPermission permission = await Geolocator.requestPermission();
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }
}
