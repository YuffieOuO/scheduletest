import 'package:flutter/material.dart';
import 'package:myapp/main.dart';
import 'home_page.dart';
import 'setting.dart';
import 'sign_in_record.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavBar extends StatefulWidget {
  const NavBar({Key? key}) : super(key: key);
  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  static final List<Widget> _widgetOptions = <Widget>[
    const HomePage(),
    const SingInRecord(),
  ];
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Punch In Out',
          style: TextStyle(fontSize: 26),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.settings),
            iconSize: 30,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const SettingPage()));
            },
          ),
          IconButton(
            icon: const Icon(Icons.logout),
            iconSize: 30,
            onPressed: () {
              logOut();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const MyApp()));
            },
          ),
        ],
      ),
      body: Center(child: _widgetOptions[_selectedIndex]),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.access_time,
              size: 34,
            ),
            label: 'Punch',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.my_library_books_outlined,
              size: 34,
            ),
            label: 'Logs',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
