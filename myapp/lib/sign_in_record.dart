import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:myapp/fetch_model.dart';
import 'package:myapp/res_model.dart';

class SingInRecord extends StatefulWidget {
  const SingInRecord({Key? key}) : super(key: key);

  @override
  _SingInRecordState createState() => _SingInRecordState();
}

class _SingInRecordState extends State<SingInRecord> {
  late Future<GetUserLogs> _userLogs;
  @override
  void initState() {
    _userLogs = FetchApi().getUserLog();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<GetUserLogs>(
        future: _userLogs,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemBuilder: (context, index) {
                print('Builde ListView');
                var log = snapshot.data!.logs[index];
                return Column(
                  children: [
                    ListTile(
                      title: Text(
                        '${log.datetime.year}-${log.datetime.month}-${log.datetime.day}',
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 23.0),
                      ),
                      trailing: Text(log.type,
                          style: const TextStyle(fontSize: 18.0)),
                      leading: Text('${index + 1}',
                          style: const TextStyle(fontSize: 18.0)),
                      subtitle: Text(
                        '${log.datetime.hour}:${log.datetime.minute}:${log.datetime.second}',
                        style: const TextStyle(fontSize: 18.0),
                      ),
                      tileColor:
                          log.type == '上班' ? Colors.grey[100] : Colors.white,
                      minVerticalPadding: 15,
                    ),
                    const Divider(
                      color: Colors.grey,
                      thickness: 1,
                      height: 10,
                    ),
                  ],
                );
              },
              itemCount: snapshot.data!.logs.length,
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        });
  }
}
